# Simple Pytest to validate that the AD2 drivers and runtime were installed properly
import pytest
import xunitparser
import dwf

# Verify that calls to the Waveforms software can be made
def test_sw_version():
    assert dwf.FDwfGetVersion() == "3.16.3"