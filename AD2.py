# Filename: AD2.py
# Author: Ari Mahpour
# Date created: 06/27/2021
# Description: Helper class to run various functions off the Analog Discovery 2 device
# Source: https://github.com/amuramatsu/dwf

import dwf
import time
import sys

class AD2:
    def __init__(self):
        # Connect to software and get version
        self.dwf_version = dwf.FDwfGetVersion()

        # Handle to analog out
        self.dwf_ao = dwf.DwfAnalogOut()

    def gen_sin_waveform(self, channel, frequency, amplitude, offset):
        self.dwf_ao.nodeEnableSet(channel, self.dwf_ao.NODE.CARRIER, True)
        self.dwf_ao.nodeFunctionSet(channel, self.dwf_ao.NODE.CARRIER, self.dwf_ao.FUNC.SINE)
        self.dwf_ao.nodeFrequencySet(channel, self.dwf_ao.NODE.CARRIER, frequency)
        self.dwf_ao.nodeAmplitudeSet(channel, self.dwf_ao.NODE.CARRIER, amplitude)
        self.dwf_ao.nodeOffsetSet(channel, self.dwf_ao.NODE.CARRIER, offset)
        self.dwf_ao.configure(channel, True)
    
    def gen_dc(self, channel, offset):
        self.dwf_ao.nodeEnableSet(channel, self.dwf_ao.NODE.CARRIER, True)
        self.dwf_ao.nodeFunctionSet(channel, self.dwf_ao.NODE.CARRIER, self.dwf_ao.FUNC.DC)
        self.dwf_ao.nodeOffsetSet(channel, self.dwf_ao.NODE.CARRIER, offset)
        self.dwf_ao.configure(channel, True)

    def stop_waveform(self, channel):
        self.dwf_ao.configure(channel, False)

    def close(self):
        self.dwf_ao.close()

## Example main to demonstrate use of functions
if __name__ == '__main__':
    import time

    CHANNEL = 1
    AMPLITUDE = 2
    OFFSET = 2

    device = AD2()
    for i in range(1000, 10000, 1000):
        print("Generating sine wave at", i, "Hz")
        device.gen_sin_waveform(CHANNEL, i, AMPLITUDE, OFFSET)
        time.sleep(1)
        device.gen_dc(CHANNEL, 0)
        time.sleep(3)

    device.close